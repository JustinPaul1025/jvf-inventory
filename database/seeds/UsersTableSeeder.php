<?php

use App\User;
use App\Types\RoleType;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate([
            'name' => 'Admin',
            'username' => 'admin',
            'password' => bcrypt('123123'),
        ]);

        $user->assignRole(RoleType::ADMINISTRATOR);

        $user = User::firstOrCreate([
            'name' => 'Staff',
            'username' => 'staff',
            'password' => bcrypt('123123'),
        ]);

        $user->assignRole(RoleType::STAFF);
    }
}
