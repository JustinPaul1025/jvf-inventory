<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reciept_id');
            $table->longText('header');
            $table->longText('footer');
            $table->unsignedBigInteger('prepared_id')->nullable();
            $table->unsignedBigInteger('verified_id')->nullable();
            $table->timestamps();

            $table->foreign('prepared_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('verified_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_receipts');
    }
}
