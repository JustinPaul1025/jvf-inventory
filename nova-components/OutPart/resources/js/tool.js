import ClassicEditor from '@ckeditor/ckeditor5-build-classic'
import VueCkeditor from 'vue-ckeditor5'
import VueSweetalert2 from 'vue-sweetalert2'
import vSelect from 'vue-select'

const options = {
  editors: {
    classic: ClassicEditor,
  },
  name: 'ckeditor'
}

Nova.booting((Vue, router, store) => {
  Vue.use(VueCkeditor.plugin, options);
  Vue.use(VueSweetalert2);
  Vue.component('v-select', vSelect);
  router.addRoutes([
    {
      name: 'out-part',
      path: '/out-part',
      component: require('./components/Tool'),
    },
    {
      name: 'out-part-create',
      path: '/out-part/create',
      component: require('./components/Outpart/Create'),
    },
  ])
})
