<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\PartsController;
use App\Http\Controllers\OutPartController;
use App\Http\Controllers\GenerateReceiptIdController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(config('app.auth_redirect'));
});

Route::middleware('auth')->group(function () {
    Route::get('users', [UserController::class, 'index']);
    
    Route::get('generate-id', [GenerateReceiptIdController::class, 'generate']);

    Route::get('parts', [PartsController::class, 'index']);
    Route::put('parts/{part}/check-stock', [PartsController::class, 'checkStock']);

    Route::post('out-part', [OutPartController::class, 'store']);
});
