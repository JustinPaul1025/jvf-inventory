<?php

namespace App\Http\Controllers;

use App\Models\Part;
use Illuminate\Http\Request;
use App\Http\Resources\PartResource;

class PartsController extends Controller
{
    public function index()
    {
        $parts = Part::get();

        return PartResource::collection($parts);
    }

    public function checkStock(Part $part, Request $request)
    {
        dd($request);
    }
}
