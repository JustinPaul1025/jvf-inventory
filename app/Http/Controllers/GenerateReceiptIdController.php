<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;

class GenerateReceiptIdController extends Controller
{
    public function generate()
    {
        $id = IdGenerator::generate(['table' => 'delivery_receipts', 'length' => 10, 'prefix' =>date('ym')]);
        $id = 'JVF-LGUDR: '.$id;
        
        return response()->json($id);
        
    }
}
