<?php

namespace App\Types;

final class RoleType
{
    const ADMINISTRATOR = 'Administrator';
    const STAFF = 'Staff';

    public static function toArray(): array
    {
        return [
            self::ADMINISTRATOR,
            self::STAFF,
        ];
    }
}