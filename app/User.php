<?php

namespace App;

use App\Types\RoleType;
use App\Models\DeliveryReceipt;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $fillable = [
        'name', 'username', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin(): bool
    {
        return $this->hasRole(RoleType::ADMINISTRATOR);
    }

    public function isStaff(): bool
    {
        return $this->hasRole(RoleType::STAFF);
    }

    public function preparedDelivery(): HasMany
    {
        return $this->hasMany(DeliveryReceipt::class, 'prepared_id');
    }

    public function verifiedDelivery(): HasMany
    {
        return $this->hasMany(DeliveryReciept::class, 'verified_id');
    }
}
