<?php

namespace App\Models;

use App\User;
use App\Models\DeliveryTable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DeliveryReceipt extends Model
{
    use LogsActivity;

    protected $fillable = [
        'receipt_id', 
        'header', 
        'footer',
        'prepared_id',
        'verified_id'
    ];

    protected static $logAttributes = ['receipt_id', 'header', 'footer', 'prepared_id', 'verified_id'];

    public function prepared(): BelongsTo
    {
        return $this->belongsTo(User::class, 'prepared_id');
    }

    public function verified(): BelongsTo
    {
        return $this->belongsTo(User::class, 'verified_id');
    }

    public function deliveryTable(): HasMany
    {
        return $this->hasMany(DeliveryTable::class);
    }
}
