<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Part extends Model
{
    use LogsActivity;

    protected $fillable = [
        'qty', 'part_number', 'description'
    ];

    protected static $logAttributes = ['qty', 'part_number', 'description'];
}
