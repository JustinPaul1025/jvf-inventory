<?php

namespace App\Models;

use App\Models\Part;
use App\Models\DeliveryTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TableItem extends Model
{
    protected $fillable = [
        'delivery_table_id', 'qty', 'part_id'
    ];

    public function deliveryTable(): BelongsTo
    {
        return $this->belongsTo(DeliveryTable::class, 'delivery_table_id');
    }

    public function part(): BelongsTo
    {
        return $this->belongsTo(Part::class, 'part_id');
    }
}
