<?php

namespace App\Models;

use App\Models\TableItem;
use App\Models\DeliveryReceipt;
use Laravel\Nova\Fields\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DeliveryTable extends Model
{
    protected $fillable = [
        'delivery_receipt_id', 'name'
    ];

    public function deliveryTable(): BelongsTo
    {
        return $this->belongsTo(DeliveryReceipt::class, 'delivery_receipt_id');
    }

    public function tableItems(): HasMany
    {
        return $this->hasMany(TableItem::class);
    }
}
