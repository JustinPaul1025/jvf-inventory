<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use App\Nova\Actions\InParts;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;

class Part extends Resource
{
    public static $model = 'App\Models\Part';

    public static $title = 'part_number';

    public static $search = [
        'id', 'part_number', 'description'
    ];

    public function fields(Request $request)
    {
        return [
            Number::make('Quantity', 'qty')->sortable(),
            Text::make('Part Number', 'part_number')->sortable(),
            Text::make('Description')->sortable()
        ];
    }

    public function cards(Request $request)
    {
        return [];
    }

    public function filters(Request $request)
    {
        return [];
    }

    public function lenses(Request $request)
    {
        return [];
    }

    public function actions(Request $request)
    {
        return [
            new InParts,
        ];
    }
}
