<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InParts extends Action
{
    use InteractsWithQueue, Queueable;

    public $showOnTableRow = true;
    public $showOnIndex = false;

    public function handle(ActionFields $fields, Collection $models)
    {
        $models[0]->update([
            'qty' => $models[0]->qty + $fields->qty
        ]);
    }

    public function fields()
    {
        return [
            Number::make('Part Quantity', 'qty')
        ];
    }
}
